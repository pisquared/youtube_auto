import os

from flask import Flask
from flask_socketio import SocketIO
from tww.tww import time_ago

from config import SQLALCHEMY_DATABASE_URI, MAKEDIRS, DATABASE_FILE

from webapp.models import db

app = Flask(__name__)
app.config["SECRET_KEY"] = "UNSECRET_KEY....478932fjkdsl"
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False




@app.template_filter()
def fmt_ts(seconds):
    return time_ago(diff=seconds)


def init_db():
    from webapp.bp_client import bp_client
    app.register_blueprint(bp_client)
    socketio.init_app(app)
    db.init_app(app)
    for dir in MAKEDIRS:
        os.makedirs(dir, exist_ok=True)
    if not os.path.exists(DATABASE_FILE):
        with app.app_context():
            db.create_all()


socketio = SocketIO(
    # logger=True,
    # engineio_logger=True,
    async_handlers=True,
    cors_allowed_origins=[
        "http://localhost:5000",
    ])
