import os
import subprocess
import sys
from time import sleep

from celery.signals import task_postrun, task_prerun
from scenedetect import VideoManager, SceneManager, ContentDetector
from scenedetect.stats_manager import StatsManager

from app import app
from config import TASKS_BUF_DIR, VIDEO_DIR, SCREENSHOT_DIR, SCENE_DIR
from webapp.models import Video, Screenshot, Scene, db
from worker import worker_app


class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


@task_prerun.connect
def before_task(task_id=None, task=None, *args, **kwargs):
    sys.stdout = Unbuffered(open(os.path.join(TASKS_BUF_DIR, task_id), 'w'))
    sys.stderr = Unbuffered(open(os.path.join(TASKS_BUF_DIR, task_id), 'w'))
    db.init_app(app)
    app.app_context().push()
    app.test_request_context().push()


@task_postrun.connect
def after_task(task_id=None, task=None, retval=None, state=None, *args, **kwargs):
    pass


@worker_app.task(bind=True, name='long_running_task')
def long_running_task(task, *args, **kwargs):
    n = 10
    for i in range(0, n + 1):
        print(i)
        sleep(1)


@worker_app.task(bind=True, name='task_detect_scenes')
def task_detect_scenes(task, *args, **kwargs):
    video_id = kwargs.get("video_id")
    video_m = Video.query.filter_by(id=video_id).first()
    scenes = detect_scene_changes(video_m)
    for i, scene in enumerate(scenes):
        start_s = scene[0].get_seconds()
        screenshot_filename = "{}_scene_{}".format(video_m.filename, i)
        take_screenshot(video_m, start_s, screenshot_filename)
        scr = Screenshot(time_s=start_s,
                         video=video_m,
                         filename=screenshot_filename,
                         )
        db.session.add(scr)
        scene = Scene(video=video_m,
                      start_s=start_s,
                      end_s=scene[1].get_seconds(),
                      screenshot=scr,
                      )
        db.session.add(scene)
    video_m.is_scenes = True
    db.session.add(video_m)
    db.session.commit()


def ffmpeg_cmd(cmd, cmd_fmt, video_m, output_dir, file_suf,
               output_filename=None, output_ext="mp4"):
    video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    if not os.path.exists(video_filepath):
        raise Exception("File {} doesn't exist".format(video_filepath))
    if not output_filename:
        output_filename = "{}_{}".format(video_m.filename, file_suf)
    output_filepath = os.path.join(output_dir,
                                   "{}.{}".format(output_filename, output_ext))
    cmd_fmt["video_fpath"] = video_filepath
    cmd_fmt["output_fpath"] = output_filepath
    cmd = cmd.format(**cmd_fmt)
    subprocess.check_output(cmd, shell=True)
    return output_filename


def take_screenshot(video_m, time_s, output_filename=None):
    cmd = "ffmpeg -y -ss {time_s} -i {video_fpath} -vframes 1 -q:v 2 {" \
          "output_fpath}"
    cmd_fmt = dict(time_s=time_s,
                   )
    return ffmpeg_cmd(cmd,
                      cmd_fmt=cmd_fmt,
                      video_m=video_m,
                      output_dir=SCREENSHOT_DIR,
                      file_suf=time_s,
                      output_filename=output_filename,
                      output_ext="jpg")


def detect_scene_changes(video_m):
    # Create a video_manager point to video file testvideo.mp4. Note that
    # multiple
    # videos can be appended by simply specifying more file paths in the list
    # passed to the VideoManager constructor. Note that appending multiple
    # videos
    # requires that they all have the same frame size, and optionally,
    # framerate.
    video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    video_manager = VideoManager([video_filepath])
    stats_manager = StatsManager()
    scene_manager = SceneManager(stats_manager)
    # Add ContentDetector algorithm (constructor takes detector options like
    # threshold).
    scene_manager.add_detector(ContentDetector())
    base_timecode = video_manager.get_base_timecode()

    scene_file_path = os.path.join(SCENE_DIR, "{}.scene.csv".format(video_m.filename))
    try:
        # If stats file exists, load it.
        if os.path.exists(scene_file_path):
            # Read stats from CSV file opened in read mode:
            with open(scene_file_path, 'r') as stats_file:
                stats_manager.load_from_csv(stats_file, base_timecode)

        # start_time = base_timecode + 20  # 00:00:00.667
        # end_time = base_timecode + 20.0  # 00:00:20.000
        # Set video_manager duration to read frames from 00:00:00 to 00:00:20.
        # video_manager.set_duration(start_time=start_time, end_time=end_time)

        # Set downscale factor to improve processing speed (no args means
        # default).
        video_manager.set_downscale_factor()

        # Start video_manager.
        video_manager.start()

        # Perform scene detection on video_manager.
        scene_manager.detect_scenes(frame_source=video_manager)

        # Obtain list of detected scenes.
        scene_list = scene_manager.get_scene_list(base_timecode)

        # We only write to the stats file if a save is required:
        if stats_manager.is_save_required():
            with open(scene_file_path, 'w') as stats_file:
                stats_manager.save_to_csv(stats_file, base_timecode)

    finally:
        video_manager.release()
    return scene_list
