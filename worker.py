from celery import Celery

from config import TASKS_IN_DIR, TASKS_PROC_DIR

worker_app = Celery(__name__)
worker_app.conf.update({
    'broker_url': 'filesystem://',
    'result_backend': "file://{}".format(TASKS_PROC_DIR),
    'broker_transport_options': {
        'data_folder_in': TASKS_IN_DIR,
        'data_folder_out': TASKS_IN_DIR,
        'data_folder_processed': TASKS_PROC_DIR,
    },
    'imports': ('tasks',),
    'result_persistent': True,
    'task_serializer': 'json',
    'result_serializer': 'json',
    'accept_content': ['json']})
