#!venv/bin/python
from app import init_db, socketio, app

if __name__ == "__main__":
    init_db()
    socketio.run(app, host='0.0.0.0', debug=True)
