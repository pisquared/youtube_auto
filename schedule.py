import subprocess
import threading

HOST = "localhost"
PORT = "1234"

PROC_VLC_CLIENT = "VLC_CLIENT"
PROC_VLC_SERVER = "VLC_SERVER"


def execute_shell_command(command):
    output = subprocess.check_output(command, shell=True)
    return output


def print_console(proc_type, message):
    print("{}: {}".format(proc_type, message))


class VLC_RC(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port

        self.thread = None
        self.process = None

    def _run_process(self):
        command = "vlc -I rc --rc-host {host}:{port}".format(host=HOST,
                                                             port=PORT)

        with subprocess.Popen(command.split(), stdout=subprocess.PIPE,
                              stderr=subprocess.STDOUT,
                              bufsize=1, universal_newlines=True) as p:
            self.process = p
            for line in self.process.stdout:
                print_console(PROC_VLC_SERVER, message=line)

        if self.process.returncode != 0:
            print_console(PROC_VLC_SERVER,
                          message="EXIT with {} - {}".format(p.returncode,
                                                             p.args))

    def start(self):
        print_console(PROC_VLC_SERVER, "starting...")
        if not self.process:
            self.thread = threading.Thread(target=self._run_process, args=(), )
            self.thread.start()
        print_console(PROC_VLC_SERVER, "started.")

    def stop(self):
        print_console(PROC_VLC_SERVER, "stopping...")
        if self.process:
            self.process.terminate()
            self.thread.join()
        print_console(PROC_VLC_SERVER, "stopped.")

    def send_vlc_command(self, command):
        print_console(PROC_VLC_SERVER, "sending...: {}".format(command))
        output = execute_shell_command(
            "echo '{command}' | netcat -N {host} {port}".format(command=command,
                                                                host=self.host,
                                                                port=self.port, ))
        # print_console(PROC_VLC_CLIENT, message=output)
        clean_output = ""
        for line in output.decode("utf-8").split('\n')[2:]:
            if "Bye-bye!" in line:
                continue
            elif line.startswith(">"):
                line = ">".join(line.split(">")[1:])
                clean_output += line.strip()
        print_console(PROC_VLC_SERVER, "received: {}".format(clean_output))
        return output

    def add_playable(self, playable):
        self.send_vlc_command("add {}".format(playable))

    def enqueue_playable(self, playable):
        self.send_vlc_command("enqueue {}".format(playable))

    def play_next(self):
        self.send_vlc_command("next")

    def _get_current_playable(self):
        status = self.send_vlc_command("status")
        for line in status.decode("utf-8").split('\r\n'):
            if "file://" in line:
                filename = line.split("file://")[1][:-2]
                break
        else:
            filename = None
        return filename

    def play_interrupt(self, playable):
        # 2. take <time_current>
        current_playable = self._get_current_playable()
        time_current = self.send_vlc_command("get_time")
        # 3. play <playable>
        self.enqueue_playable(playable)
        self.play_next()
        self.enqueue_playable(current_playable)
        # 4. play previous from <time_current>
        # self.send_vlc_command("seek {}".format(time_current))


def main():
    vlc_rc = VLC_RC(HOST, PORT)
    vlc_rc.start()
    while True:
        c = input("> ")
        if len(c) < 1:
            continue
        spl_com = c.split()
        com, args = spl_com[0], spl_com[1:]
        if com == "s":
            vlc_rc.stop()
        elif com == "r":
            vlc_rc.add_playable(
                "/usr/local/google/home/pisquared/cb/ff/ff_4d_bloopers.mkv")
        elif com == "p":
            vlc_rc.send_vlc_command("pause")
        elif com == "a":
            vlc_rc.play_interrupt(
                "/usr/local/google/home/pisquared/cb/ff_chopped"
                "/ff_lhc_bloopers_000.mp4")
        elif com == "e":
            vlc_rc.stop()
            break


if __name__ == "__main__":
    main()
