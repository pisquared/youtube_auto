import os

from flask_sqlalchemy import SQLAlchemy
from kombu.utils import json
from sqlalchemy_utils import ChoiceType

from config import VIDEO_DIR_NAME, SCREENSHOT_DIR_NAME, THUMBNAIL_DIR_NAME


db = SQLAlchemy()


class Video(db.Model):
    SOURCE_TYPE_YOUTUBE = "YOUTUBE"
    SOURCE_TYPE_MOVE = "MOVE"
    SOURCE_TYPE_CUT = "CUT"
    SOURCE_TYPE_INTRO_OUTRO = "INTRO_OUTRO"
    SOURCE_TYPE_UPLOAD = "UPLOAD"

    SOURCE_TYPES = [
        (SOURCE_TYPE_YOUTUBE, u'youtube'),
        (SOURCE_TYPE_MOVE, u'move'),
        (SOURCE_TYPE_CUT, u'cut'),
        (SOURCE_TYPE_INTRO_OUTRO, u'intro_outro'),
        (SOURCE_TYPE_UPLOAD, u'upload'),
    ]

    id = db.Column(db.Integer, primary_key=True)

    youtube_vid = db.Column(db.Unicode)
    title = db.Column(db.Unicode)
    description = db.Column(db.Unicode)
    duration = db.Column(db.Integer)

    filename = db.Column(db.Unicode)
    fileext = db.Column(db.Unicode)

    is_scenes = db.Column(db.Boolean)

    set_thumbnail_id = db.Column(db.Integer, db.ForeignKey('thumbnail.id'))
    set_thumbnail = db.relationship('Thumbnail', foreign_keys=[set_thumbnail_id])

    source_type = db.Column(ChoiceType(SOURCE_TYPES))
    source_video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    source_video = db.relationship('Video', backref=db.backref('produced_videos'), remote_side=[id])

    @property
    def full_filename(self):
        return "{}.{}".format(self.filename, self.fileext)

    @property
    def serve_path(self):
        return os.path.join(VIDEO_DIR_NAME, self.full_filename)

    def __repr__(self):
        return '<Video %r>' % self.filename


class TimeDescription(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    time_s = db.Column(db.Integer)
    description = db.Column(db.Unicode)

    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    video = db.relationship('Video', backref=db.backref('time_descriptions'))

    screenshot_id = db.Column(db.Integer, db.ForeignKey('screenshot.id'))
    screenshot = db.relationship('Screenshot', backref=db.backref('time_description', uselist=False))


class Scene(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    start_s = db.Column(db.Integer)
    end_s = db.Column(db.Integer)
    description = db.Column(db.Unicode)

    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    video = db.relationship('Video', backref=db.backref('scenes'))

    screenshot_id = db.Column(db.Integer, db.ForeignKey('screenshot.id'))
    screenshot = db.relationship('Screenshot', backref=db.backref('scene', uselist=False))


class Screenshot(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    time_s = db.Column(db.Integer)
    filename = db.Column(db.Unicode)

    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    video = db.relationship('Video', backref=db.backref('screenshots'))

    @property
    def full_filename(self):
        return "{}.jpg".format(self.filename)

    @property
    def serve_path(self):
        return os.path.join(SCREENSHOT_DIR_NAME, self.full_filename)


class Thumbnail(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    time_s = db.Column(db.Integer)
    filename = db.Column(db.Unicode)

    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    video = db.relationship('Video', foreign_keys=[video_id], backref=db.backref('thumbnails'))

    @property
    def full_filename(self):
        return "{}.jpg".format(self.filename)

    @property
    def serve_path(self):
        return os.path.join(THUMBNAIL_DIR_NAME, self.full_filename)


class Font(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    family = db.Column(db.Unicode)
    style = db.Column(db.Unicode)
    filename = db.Column(db.Unicode)
    short_filename = db.Column(db.Unicode)


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Unicode)
    uuid = db.Column(db.Unicode)
    kwargs = db.Column(db.Unicode)

    def serialize(self):
        return dict(
            name=self.name, uuid=self.uuid, kwargs=json.loads(self.kwargs),
        )
