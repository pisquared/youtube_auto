function gototime(seconds) {
    var videoEl = $("video");
    if (videoEl) {
        videoEl = videoEl[0];
        videoEl.currentTime = seconds;
        videoEl.play();
    }
}

function changeRate(selection) {
    var videoEl = $("video");
    if (videoEl) {
        videoEl = videoEl[0];
        var rate = $('#steplist option:nth-child(' + (selection) + ')').data("speed");
        videoEl.playbackRate = rate;
        videoEl.play();
        $('.current-rate').each(function () {
            $(this).text(rate);
        });
    }
}

function seconds_to_human(seconds) {
    if (seconds < 3600)
        return new Date(seconds * 1000).toISOString().substr(14, 5);
    return new Date(seconds * 1000).toISOString().substr(11, 8);
}

function human_to_seconds(hms) {
    var a = hms.split(':');
    if (a.length === 3) {
        return (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    }
    return (+a[0]) * 60 + (+a[1]);
}

$(".gototime").click(function (e) {
    e.preventDefault();
    gototime($(this).data('seconds'));
});