from flask import Blueprint

bp_client = Blueprint('client', __name__,
                      static_folder='static',
                      static_url_path='/static/client',
                      template_folder='templates')


from webapp.bp_client import views
