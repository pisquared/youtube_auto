import json
import os
import subprocess
from shutil import copyfile, move
from time import time
from urllib.parse import urlparse, parse_qs

import cv2
import ffmpeg
import youtube_dl
from PIL import Image, ImageDraw, ImageFont
from flask import render_template, redirect, url_for, request, flash, \
    send_from_directory, jsonify
from flask_socketio import emit
from flask_table import Table, Col, LinkCol
from pydub import AudioSegment

from config import SERVE_DIR, HAAR_DIR, VIDEO_DIR, VIDEO_TEMPLATES_DIR, \
    IMAGES_DIR, \
    SCREENSHOT_DIR, THUMBNAIL_DIR, \
    AUDIO_DIR, DEFAULT_CATEGORY, DEFAULT_KEYWORDS, TASKS_BUF_DIR
from tasks import ffmpeg_cmd, take_screenshot
from upload_video import upload_to_youtube
from webapp.bp_client import bp_client
from app import socketio
from webapp.models import db, Video, TimeDescription, Scene, Screenshot, Thumbnail, Font, Task
from worker import worker_app


def yd_progress_hook(d):
    if d['status'] == 'finished':
        print("Done downloading {}".format(d['filename']))
    if d['status'] == 'downloading':
        print(d['filename'], d['_percent_str'], d['_eta_str'])


class YDLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def download_video(youtube_url, filename):
    ydl_opts = {
        'outtmpl': os.path.join(VIDEO_DIR, "{}.%(ext)s".format(filename)),
        'forcejson': True,
        'logger': YDLogger(),
        'progress_hooks': [yd_progress_hook],
    }
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        info_dict = ydl.extract_info(youtube_url, download=True)
        return info_dict


def find_fileext(filename):
    import os
    for f in os.listdir(VIDEO_DIR):
        if f.startswith(filename):
            return f.split(".")[-1]


def is_youtube_url(name):
    if name.startswith("http://") or name.startswith("https://"):
        name = "://".join(name.split("://")[1:])
    if name.startswith("www."):
        name = "www.".join(name.split("www.")[1:])
    for prefix in ["youtu.be", "youtube."]:
        if name.startswith(prefix):
            return True
    return False


def url_to_youtube_vid(youtube_url):
    url_parsed = urlparse(youtube_url)
    if "youtu.be" in url_parsed.hostname:
        return url_parsed.path
    if url_parsed.query is None:
        raise Exception("Not a valid youtube video page")
    try:
        qs = parse_qs(url_parsed.query)
        v = qs["v"][0]
    except:
        raise Exception("Not a valid youtube video page")
    return v


def check_download_video(youtube_url, filename):
    if not is_youtube_url(youtube_url):
        raise Exception("Not a valid Youtube URL: {}".format(youtube_url))
    youtube_vid = url_to_youtube_vid(youtube_url)
    video_m = Video.query.filter_by(youtube_vid=youtube_vid).first()
    if not video_m:
        video_info = download_video(youtube_url, filename)
        fileext = find_fileext(filename)
        video_m = Video(youtube_vid=video_info.get("id"),
                        filename=filename,
                        fileext=fileext,
                        title=video_info.get("title"),
                        description=video_info.get("description"),
                        duration=video_info.get("duration"),
                        source_type=Video.SOURCE_TYPE_YOUTUBE,
                        )
        db.session.add(video_m)
        db.session.commit()
        video_m = Video.query.filter_by(filename=filename).first()
    return video_m


def extract_audio(video_m):
    audio_filepath = os.path.join(AUDIO_DIR, "{}.mp3".format(video_m.filename))
    if os.path.exists(audio_filepath):
        return
    video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    subprocess.check_output("ffmpeg -y -i {} {}".format(video_filepath,
                                                        audio_filepath),
                            shell=True)


def cut_video_ffmpeg(video_m, start_time_s, end_time_s, output_filename=None):
    cmd = "ffmpeg -y -i {video_fpath} -ss {start_time} -to {end_time} {" \
          "output_fpath}"
    cmd_fmt = dict(start_time=start_time_s,
                   end_time=end_time_s,
                   )
    return ffmpeg_cmd(cmd,
                      cmd_fmt=cmd_fmt,
                      video_m=video_m,
                      output_dir=VIDEO_DIR,
                      file_suf="{}_{}".format(start_time_s, end_time_s),
                      output_filename=output_filename,
                      output_ext=video_m.fileext)


def generate_intro_outro_video(video_m, intro_type, outro_type,
                               output_filename=None):
    main_video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    if not os.path.exists(main_video_filepath):
        raise Exception("File {} doesn't exist".format(main_video_filepath))

    if not intro_type and not outro_type:
        raise Exception("At least an intro or outro is needed")

    if not output_filename:
        output_filename = video_m.filename
        if intro_type:
            output_filename = "{}_{}".format(output_filename, intro_type)
        if outro_type:
            output_filename = "{}_{}".format(output_filename, outro_type)
    output_filepath = os.path.join(VIDEO_DIR, "{}.mp4".format(output_filename))

    concats = []
    if intro_type:
        intro_video_filepath = os.path.join(VIDEO_TEMPLATES_DIR,
                                            "{}.mp4".format(intro_type))
        if not os.path.exists(intro_video_filepath):
            raise Exception(
                "File intro {} doesn't exist".format(intro_video_filepath))
        istream = ffmpeg.input(intro_video_filepath)
        concats += [istream.video, istream.audio]

    mstream = ffmpeg.input(main_video_filepath)
    # tmp_vm_name = os.path.join(TMP_DIR, "{}_tmp.mp4".format(output_filename))

    # rescale to match i/o format
    vm_tmp = mstream.video.filter('scale', "1920", -1).filter("fps", "24")

    concats += [vm_tmp, mstream.audio]

    if outro_type:
        outro_video_filepath = os.path.join(VIDEO_TEMPLATES_DIR,
                                            "{}.mp4".format(outro_type))
        if not os.path.exists(outro_video_filepath):
            raise Exception(
                "File outro {} doesn't exist".format(outro_video_filepath))
        ostream = ffmpeg.input(outro_video_filepath)
        concats += [ostream.video, ostream.audio]

    ffmpeg.concat(*concats, v=1, a=1).output(output_filepath).global_args(
        '-y').run()
    return output_filename


def text_wrap(text, font, max_width):
    lines = []
    # If the width of the text is smaller than image width
    # we don't need to split it, just add it to the lines array
    # and return
    if font.getsize(text)[0] <= max_width:
        lines.append(text)
    else:
        # split the line by spaces to get words
        words = text.split(' ')
        i = 0
        # append every word to a line while its width is shorter than image
        # width
        while i < len(words):
            line = ''
            while i < len(words) and font.getsize(line + words[i])[
                0] <= max_width:
                line = line + words[i] + " "
                i += 1
            if not line:
                line = words[i]
                i += 1
            # when the line gets longer than the max width do not append the
            # word,
            # add the line to the lines array
            lines.append(line)
    return lines


def draw_text(draw, text, font, pos, area, fill):
    lines = text_wrap(text, font, area[0])
    line_height = font.getsize('hg')[1]
    x, y = pos
    for line in lines:
        # draw the line on the image
        draw.text((x, y), line, fill=fill, font=font)
        # update the y position so that we can use it for next line
        y = y + line_height


def generate_thumbnail(bg_img_fpath, opts, output_filename=None):
    image = Image.open(bg_img_fpath)
    image = image.resize((2000, 1135), Image.ANTIALIAS)

    draw = ImageDraw.Draw(image)
    label_fg = opts.get("label", {}).get("fg", "#00f")
    label_bg = opts.get("label", {}).get("bg", "#f00")
    label_pos1 = opts.get("label", {}).get("pos1", [640, 670])
    label_pos2 = opts.get("label", {}).get("pos2", [1990, 970])
    label_padding = opts.get("label", {}).get("padding", 20)
    label_text = opts.get("label", {}).get("text", "Curious Bunch")
    label_font = opts.get("label", {}).get("font", "arial")
    label_font_size = opts.get("label", {}).get("font_size", 130)
    font = ImageFont.truetype(label_font, size=label_font_size)
    draw.rectangle((tuple(label_pos1), tuple(label_pos2)), fill=label_bg)

    text_area = [label_pos2[0] - label_pos1[0] - 2 * label_padding, ]
    text_draw_pos = [x + label_padding for x in label_pos1]
    draw_text(draw, label_text, font, text_draw_pos, text_area, label_fg)

    for watermark in opts.get("watermarks", []):
        img_filename = watermark.get('filename', "cb_logo.png")
        position = watermark.get('pos', [0, 0])
        wm_image = Image.open(os.path.join(IMAGES_DIR, img_filename))
        image.paste(wm_image, (tuple(position)), wm_image)

    # save the edited image
    if not output_filename:
        output_filename = '.'.join(
            os.path.basename(bg_img_fpath).split('.')[:-1])
    output_filepath = os.path.join(THUMBNAIL_DIR,
                                   "{}.jpg".format(output_filename))
    image.save(output_filepath)

    return output_filename


def generate_image_list_fonts():
    pangram = "За миг бях в чужд, скърцащ плюшен фотьойл. | The quick brown fox jumps over the lazy dog | 01234567890"
    command = "fc-list -f '%{family[0]} # %{style[0]} # %{file}\\n' :lang=bg | sort"
    font_names = [x.split('#') for x in str(subprocess.check_output(command, shell=True))[2:].split('\\n')][:-1]
    parent_font_family, parent_font_style, img_height = "", "", 0
    for font_family, font_style, font_filename in font_names:
        if font_family != parent_font_family:
            img_height += 30
            parent_font_family = font_family
        if font_style != parent_font_style:
            img_height += 20
            parent_font_style = font_style
    img = Image.new('RGB', (1000, img_height), color='white')
    draw = ImageDraw.Draw(img)
    y = 0
    parent_font_family, parent_font_style = "", ""
    for font_family, font_style, font_filename in font_names:
        font_family, font_style, font_filename = font_family.strip(), font_style.strip(), font_filename.strip()
        short_font_filename = os.path.basename(font_filename)
        if font_family != parent_font_family:
            try:
                font = ImageFont.truetype(font_filename, size=20)
                draw.text((0, y), short_font_filename, fill="blue", font=font)
            except Exception as e:
                font = ImageFont.truetype("arial", size=15)
                draw.text((0, y), " {}: failed to draw".format(short_font_filename), fill="red", font=font)
            y += 30
            parent_font_family = font_family
        font = ImageFont.truetype(font_filename, size=14)
        if font_style != parent_font_style:
            draw.text((0, y), "{}: {}".format(font_style, pangram), fill="black", font=font)
            y += 20
            parent_font_style = font_style
            f = Font.query.filter_by(family=font_family, style=font_style,
                                     filename=font_filename, short_filename=short_font_filename).first()
            if not f:
                f = Font(family=font_family, style=font_style, filename=font_filename,
                         short_filename=short_font_filename)
                db.session.add(f)
    db.session.commit()
    output_filepath = os.path.join(IMAGES_DIR, "fonts.png")
    img.save(output_filepath)


def normalize_audio(video_m):
    def match_target_amplitude(sound, target_dBFS):
        change_in_dBFS = target_dBFS - sound.dBFS
        return sound.apply_gain(change_in_dBFS)

    audio_filepath = os.path.join(AUDIO_DIR,
                                  "{}.mp3".format(video_m.filename))
    audio_norm_filepath = os.path.join(AUDIO_DIR,
                                       "{}_norm.mp3".format(video_m.filename))
    sound = AudioSegment.from_file(audio_filepath, "mp3")
    normalized_sound = match_target_amplitude(sound, -20.0)
    normalized_sound.export(audio_norm_filepath, format="mp3")


def normalize_audio_2(video_m):
    audio_filepath = os.path.join(AUDIO_DIR, "{}.mp3".format(video_m.filename))
    audio_norm_filepath = os.path.join(AUDIO_DIR,
                                       "{}_norm2.mp3".format(video_m.filename))
    subprocess.check_output(
        "ffmpeg-normalize {} -pr -c:a libmp3lame -b:a 320k -o {}".format(
            audio_filepath,
            audio_norm_filepath,
        ))


face_cascade = cv2.CascadeClassifier(
    os.path.join(HAAR_DIR, 'haarcascade_frontalface_default.xml'))
eye_cascade = cv2.CascadeClassifier(
    os.path.join(HAAR_DIR, 'haarcascade_eye.xml'))
smile_cascade = cv2.CascadeClassifier(
    os.path.join(HAAR_DIR, 'haarcascade_smile.xml'))


def detect(gray, frame):
    smiles_d = {}
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)
    for faceid, (x, y, w, h) in enumerate(faces):
        roi_gray = gray[y:y + h, x:x + w]
        smiles_d[faceid] = False
        smiles = smile_cascade.detectMultiScale(roi_gray, 1.8, 20)
        if len(smiles) > 0:
            smiles_d[faceid] = True
    return smiles_d


def detect_smiles(video_m):
    SAMPLE_EVERY_FRAME = 10
    video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    # video_filepath = os.path.join(VIDEO_DIR, "bg01_sample.mp4")
    video_capture = cv2.VideoCapture(video_filepath)
    frame_id = 0
    frame_smiles = []
    start_time = time()
    while True:
        # Captures video_capture frame by frame
        _, frame = video_capture.read()

        # calls the detect() function
        frame_id += 1
        if frame_id % SAMPLE_EVERY_FRAME == 0:
            # To capture image in monochrome
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            smiles = detect(gray, frame)
            face_count = len(smiles)
            smile_count = len([v for v in smiles.values() if v])
            d_seconds = time() - start_time
            sample_nu = frame_id // SAMPLE_EVERY_FRAME,
            print("[{:1.2f}] sample: {}: faces: {} smiles: {}".format(d_seconds,
                                                                      sample_nu,
                                                                      face_count,
                                                                      smile_count))
            start_time = time()


@bp_client.route("/")
def home():
    return redirect(url_for("client.import_video"))


class VideosTable(Table):
    title = Col('title')
    filename = Col('filename')
    timeit = LinkCol('TimeIt', "client.timeit_video", url_kwargs=dict(video_id='id'))
    cut = LinkCol('Cut', "client.cut_video", url_kwargs=dict(video_id='id'))
    intro_outro = LinkCol('Intro/Outro', "client.intro_outro_video",
                          url_kwargs=dict(video_id='id'))
    thumbnail = LinkCol('Thumb', "client.thumbnail_video",
                        url_kwargs=dict(video_id='id'))
    delete = LinkCol('Del', "client.delete_video", url_kwargs=dict(video_id='id'))


@bp_client.route("/download", methods=["POST"])
def download():
    youtube_url = request.form.get("youtube_url")
    filename = request.form.get("filename")
    if not (youtube_url and filename):
        flash("You need to provide both youtube_vid and a filename",
              "error")
        return redirect(url_for("client.download",
                                youtube_url=youtube_url,
                                filename=filename))
    try:
        check_download_video(youtube_url, filename)
    except Exception as e:
        flash(str(e), "error")
        return redirect(url_for("client.import_video",
                                youtube_url=youtube_url,
                                filename=filename))
    return redirect(url_for("client.import_video"))


@bp_client.route("/import_video", methods=["GET", "POST"])
def import_video():
    if request.method == "POST":
        old_filepath = request.form.get("old_filepath")
        new_filename = request.form.get("new_filename")
        is_move = bool(request.form.get("is_move", False))

        fileext = old_filepath.split('.')[-1]
        if not new_filename:
            filename = '.'.join(os.path.basename(old_filepath).split('.')[:-1])
        else:
            filename = new_filename
        new_filepath = os.path.join(VIDEO_DIR, "{}.{}".format(filename, fileext))
        video_m = Video(filename=filename,
                        fileext=fileext,
                        source_type=Video.SOURCE_TYPE_MOVE,
                        )
        db.session.add(video_m)
        db.session.commit()
        try:
            if is_move:
                move(old_filepath, new_filepath)
                verb = "moved"
            else:
                copyfile(old_filepath, new_filepath)
                verb = "copied"
            flash(
                "Successfully {} {} to {}".format(verb, old_filepath, new_filepath),
                "success")
        except Exception as e:
            flash(str(e), "error")
    videos = Video.query.all()
    videos_table = VideosTable(videos)
    return render_template("import_video.html", videos_table=videos_table)


class TimeitVideoTable(Table):
    id = LinkCol('TimeIt', "client.timeit_video", url_kwargs=dict(video_id='id'))
    title = Col('title')
    filename = Col('filename')


@bp_client.route("/timeit", methods=["GET"])
def timeit():
    videos = Video.query.all()
    videos_table = TimeitVideoTable(videos)
    return render_template("timeit.html", videos_table=videos_table)


@bp_client.route("/time_description/<int:video_id>/add", methods=["POST"])
def add_time_description(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(request.referrer or url_for("client.home"))
    is_async = request.form.get("is_async")
    time_s = request.form.get("time")
    description = request.form.get("description")
    screenshot_filename = "{}_time_{}".format(video_m.filename, time_s)
    take_screenshot(video_m, time_s, screenshot_filename)
    scr = Screenshot(time_s=time_s,
                     video=video_m,
                     filename=screenshot_filename,
                     )
    db.session.add(scr)
    td = TimeDescription(time_s=time_s,
                         description=description,
                         video=video_m,
                         screenshot=scr,
                         )
    db.session.add(td)
    db.session.commit()
    if not is_async:
        flash("Created time description", "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/timeit/<int:video_id>", methods=["GET", "POST"])
def timeit_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    time_descriptions = TimeDescription.query.filter_by(video=video_m).order_by(TimeDescription.time_s).all()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.timeit"))

    return render_template("timeit_video.html", video_m=video_m, time_descriptions=time_descriptions)


@bp_client.route("/detect_scenes_video/<int:video_id>", methods=["POST"])
def detect_scenes_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.timeit"))
    if video_m.is_scenes:
        flash("Scenes already detected", "info")
        return redirect(url_for("client.timeit_video", video_id=video_id))
    try:
        async_task_id = worker_start_task('task_detect_scenes', dict(video_id=video_id))
        flash("submitted task {}".format(async_task_id), "success")
    except Exception as e:
        flash(str(e), "error")
    return redirect(url_for("client.timeit_video", video_id=video_id))


class CutVideoTable(Table):
    id = LinkCol('Cut', "client.cut_video", url_kwargs=dict(video_id='id'))
    title = Col('title')
    filename = Col('filename')


@bp_client.route("/cut", methods=["GET"])
def cut():
    videos = Video.query.all()
    videos_table = CutVideoTable(videos)
    return render_template("cut.html", videos_table=videos_table)


def parse_time_duration(time):
    try:
        if ':' in time:
            splitted_time = time.split(':')
            if len(splitted_time) == 2:
                # assume mm:ss[.fff]
                mins, secs = splitted_time
                hours, mins, secs = 0, int(mins), float(secs)
            elif len(splitted_time) == 3:
                hours, mins, secs = splitted_time
                hours, mins, secs = int(hours), int(mins), float(secs)
            else:
                raise Exception()
            assert 0 < mins < 60
            assert 0 < secs < 60
            return 3600 * hours + 60 * mins + secs
        # assume it's seconds
        return float(time)
    except:
        raise Exception("Incorrect time format: {}".format(time))


@bp_client.route("/cut/<int:video_id>", methods=["GET", "POST"])
def cut_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    time_descriptions = TimeDescription.query.filter_by(video=video_m).order_by(TimeDescription.time_s).all()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.cut"))
    if request.method == "POST":
        start_time = request.form.get("start_time")
        end_time = request.form.get("end_time")
        if start_time is None or end_time is None:
            flash("You need to provide both start_time and a end_time", "error")
            return redirect(url_for("client.cut_video",
                                    video_id=video_id,
                                    start_time=start_time,
                                    end_time=end_time))
        start_time_s = parse_time_duration(start_time)
        end_time_s = parse_time_duration(end_time)
        output_filename = request.form.get("output_filename")
        try:
            filename = cut_video_ffmpeg(video_m, start_time_s,
                                        end_time_s,
                                        output_filename=output_filename)
            video_new = Video(filename=filename,
                              fileext=video_m.fileext,
                              source_type=Video.SOURCE_TYPE_CUT,
                              source_video=video_m,
                              )
            db.session.add(video_new)
            db.session.commit()
            flash("Success", "success")
        except Exception as e:
            flash(str(e), "error")
        return redirect(url_for("client.cut_video", video_id=video_id))
    return render_template("cut_video.html", video_m=video_m, time_descriptions=time_descriptions)


@bp_client.route("/media/<path:filename>")
def serve_media(filename):
    return send_from_directory(SERVE_DIR, filename)


class IntroOutroVideoTable(Table):
    id = LinkCol('IntroOutro', "client.intro_outro_video",
                 url_kwargs=dict(video_id='id'))
    title = Col('title')
    filename = Col('filename')


@bp_client.route("/intro_outro", methods=["GET"])
def intro_outro():
    videos = Video.query.all()
    videos_table = IntroOutroVideoTable(videos)
    return render_template("intro_outro.html", videos_table=videos_table)


@bp_client.route("/intro_outro/<int:video_id>", methods=["GET", "POST"])
def intro_outro_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.intro_outro"))
    return render_template("intro_outro_video.html", video_m=video_m)


@bp_client.route("/gen_intro_outro_video/<int:video_id>", methods=["POST"])
def gen_intro_outro_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(request.referrer or url_for("client.home"))
    is_async = request.form.get("is_async")
    intro_type = request.form.get("intro_type")
    outro_type = request.form.get("outro_type")
    output_filename = request.form.get("output_filename")
    try:
        filename = generate_intro_outro_video(video_m,
                                              intro_type,
                                              outro_type,
                                              output_filename)
    except Exception as e:
        flash(str(e), "error")
        return redirect(request.referrer or url_for("client.home"))
    scr = Video(filename=filename,
                fileext="mp4",
                source_type=Video.SOURCE_TYPE_INTRO_OUTRO,
                source_video=video_m,
                )
    db.session.add(scr)
    db.session.commit()
    if not is_async:
        flash("Created intro/outro", "success")
    return redirect(request.referrer or url_for("client.home"))


class ThumbnailVideoTable(Table):
    id = LinkCol('Thumbnail', "client.thumbnail_video", url_kwargs=dict(video_id='id'))
    title = Col('title')
    filename = Col('filename')


@bp_client.route("/thumbnail", methods=["GET"])
def thumbnail():
    videos = Video.query.all()
    videos_table = ThumbnailVideoTable(videos)
    return render_template("thumbnail.html", videos_table=videos_table)


@bp_client.route("/thumbnail/<int:video_id>", methods=["GET", "POST"])
def thumbnail_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.thumbnail"))
    fonts = Font.query.all()
    return render_template("thumbnail_video.html", video_m=video_m, fonts=fonts)


@bp_client.route("/take_screenshot/<int:video_id>", methods=["POST"])
def take_screenshot_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(request.referrer or url_for("client.home"))
    is_async = request.form.get("is_async")
    time_s = request.form.get("time")
    output_filename = request.form.get("output_filename")
    try:
        filename = take_screenshot(video_m, time_s, output_filename)
    except Exception as e:
        flash(str(e), "error")
        return redirect(request.referrer or url_for("client.home"))
    scr = Screenshot(time_s=time_s,
                     video=video_m,
                     filename=filename,
                     )
    db.session.add(scr)
    db.session.commit()
    if not is_async:
        flash("Created screenshot", "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/gen_thumbnail/<int:video_id>", methods=["POST"])
def gen_thumbnail_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.cut"))
    is_async = request.form.get("is_async")
    bg_img_type = request.form.get("bg_img_type")  # image/screenshot
    bg_img_fname = request.form.get("bg_img_fname")

    if bg_img_type == "screenshot":
        bg_img_fpath = os.path.join(SCREENSHOT_DIR,
                                    "{}.jpg".format(bg_img_fname))
    elif bg_img_type == "image":
        bg_img_fpath = os.path.join(IMAGES_DIR, "{}.jpg".format(bg_img_fname))
    else:
        flash("bg_img_type must be either screenshot or image", "error")
        return redirect(request.referrer or url_for("client.home"))
    label_pos = request.form.get("label_pos", "640:670-1990:970")
    label_pos1, label_pos2 = label_pos.split('-')
    opts = {
        "label": {
            "text": request.form.get("label_text", "Curious Bunch"),
            "font": request.form.get("label_font", "arial"),
            "font_size": int(request.form.get("label_font_size", 120)),
            "pos1": [int(x) for x in label_pos1.split(":")],
            "pos2": [int(x) for x in label_pos2.split(":")],
            "padding": int(request.form.get("label_padding", 20)),
            "fg": request.form.get("label_fg_color", "#eee"),
            "bg": request.form.get("label_bg_color", "#000"),
        },
        "watermarks": [{
            "filename": "cb_logo.png",
            "pos": [0, 560],
            "size": [200, 200],
        }],
    }
    output_filename = request.form.get("output_filename")
    try:
        filename = generate_thumbnail(bg_img_fpath, opts, output_filename)
    except Exception as e:
        flash(str(e), "error")
        return redirect(request.referrer or url_for("client.home"))
    thmb = Thumbnail(video=video_m,
                     filename=filename,
                     )
    db.session.add(thmb)
    db.session.commit()
    if not is_async:
        flash("Created thumbnail", "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route('/generate_system_fonts')
def generate_fonts():
    try:
        generate_image_list_fonts()
        flash("Generated system fonts", "success")
    except Exception as e:
        flash(str(e), "error")
        return redirect(request.referrer or url_for("client.home"))
    return redirect(request.referrer or url_for("client.thumbnail"))


@bp_client.route("/thumbnail_delete/<int:thumb_id>")
def thumb_delete(thumb_id):
    thumb_m = Thumbnail.query.filter_by(id=thumb_id).first()
    if not thumb_m:
        flash("Thumbnail {} doesn't exist".format(thumb_id), "error")
        return redirect(url_for("client.home"))
    filepath = os.path.join(THUMBNAIL_DIR, thumb_m.full_filename)
    try:
        os.remove(filepath)
        flash("Deleted thumbfile {}".format(thumb_m.id), "success")
    except Exception as e:
        flash(str(e), "error")

    db.session.delete(thumb_m)
    db.session.commit()
    flash("Deleted thumbnail {}".format(thumb_id), "success")
    return redirect(request.referrer or url_for("client.home"))


class UploadVideoTable(Table):
    id = LinkCol('Upload', "client.upload_video", url_kwargs=dict(video_id='id'))
    title = Col('title')
    filename = Col('filename')


@bp_client.route("/upload", methods=["GET"])
def upload():
    videos = Video.query.all()
    videos_table = UploadVideoTable(videos)
    return render_template("upload.html", videos_table=videos_table)


def upload_and_set(video_m, title, description, thumb, playlist, privacy):
    video_filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
    if not os.path.exists(video_filepath):
        raise Exception("Video {} doesn't exist".format(video_filepath))
    thumb_fpath = os.path.join(THUMBNAIL_DIR, thumb.full_filename)
    if not os.path.exists(thumb_fpath):
        raise Exception("Thumbnail {} doesn't exist".format(video_filepath))
    return upload_to_youtube(video_filepath,
                             title=title,
                             description=description,
                             category=DEFAULT_CATEGORY,
                             keywords=DEFAULT_KEYWORDS,
                             playlist=playlist,
                             playlist_pos=0,
                             privacy=privacy,
                             thumbnail_file=thumb_fpath,
                             )


@bp_client.route("/upload/<int:video_id>", methods=["GET", "POST"])
def upload_video(video_id):
    if not os.path.exists("client_secrets.json"):
        flash("client_secrets.json doesn't exist, please see README step 3.", "error")
        return redirect(request.referrer or url_for("client.home"))
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(request.referrer or url_for("client.home"))
    if video_m.source_type == Video.SOURCE_TYPE_UPLOAD and video_m.youtube_vid:
        return redirect(url_for("client.youtube_manage", video_id=video_m.id))
    if request.method == "POST":
        title = request.form.get("title")
        description = request.form.get("description")
        thumbnail_id = request.form.get("thumbnail_id")
        playlist_id = request.form.get("playlist_id")
        privacy = request.form.get("privacy")

        thumbnail_m = Thumbnail.query.filter_by(id=thumbnail_id).first()
        if not thumbnail_m:
            flash("Thumbnail {} doesn't exist".format(thumbnail_id), "error")
            return redirect(request.referrer or url_for("client.home"))
        try:
            youtube_video_id = upload_and_set(video_m, title, description,
                                              thumbnail_m, playlist_id, privacy)
            video_m.title = title
            video_m.description = description
            video_m.youtube_vid = youtube_video_id
            video_m.source_type = Video.SOURCE_TYPE_UPLOAD
            video_m.set_thumbnail = thumbnail_m
            db.session.add(video_m)
            db.session.commit()
            flash("Successfully uploaded video: {}".format(youtube_video_id),
                  "success")
        except Exception as e:
            flash(str(e), "error")
            return render_template("upload_video.html", video_m=video_m)
        return redirect(url_for("client.youtube_manage", video_id=video_m.id))
    return render_template("upload_video.html", video_m=video_m)


@bp_client.route('/youtube_manage/<int:video_id>')
def youtube_manage(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(request.referrer or url_for("client.home"))
    if not video_m.youtube_vid:
        flash("video {} doesn't have associated youtube_vid", "error")
        return redirect(request.referrer or url_for("client.home"))
    return render_template("youtube_manage_video.html", video_m=video_m)


@bp_client.route("/extract_audio", methods=["GET", "POST"])
def extract_audio():
    return render_template("home.html")


@bp_client.route("/videos/<int:video_id>/delete", methods=["GET", "POST"])
def delete_video(video_id):
    video_m = Video.query.filter_by(id=video_id).first()
    if not video_m:
        flash("Video {} doesn't exist".format(video_id), "error")
        return redirect(url_for("client.home"))
    if request.method == "POST":
        also_videofile = bool(request.form.get("also_videofile"))
        if also_videofile:
            filepath = os.path.join(VIDEO_DIR, video_m.full_filename)
            try:
                os.remove(filepath)
                flash("Deleted videofile {}".format(video_m.id), "success")
            except Exception as e:
                flash(str(e), "error")
        db.session.delete(video_m)
        db.session.commit()
        flash("Deleted video {}".format(video_id), "success")
        return redirect(url_for("client.home"))
    return render_template("confirm_delete.html", video_m=video_m)


@bp_client.route("/scene/<int:scene_id>/edit", methods=["POST"])
def scene_edit(scene_id):
    scene_m = Scene.query.filter_by(id=scene_id).first()
    if not scene_m:
        flash("Scene {} doesn't exist".format(scene_id), "error")
        return redirect(request.referrer, url_for("client.home"))
    description = request.form.get("description")
    scene_m.description = description
    db.session.add(scene_m)
    db.session.commit()
    flash("Scene {} edited".format(scene_id), "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/scenes/<int:scene_id>/delete", methods=["GET", "POST"])
def scene_delete(scene_id):
    scene_m = Scene.query.filter_by(id=scene_id).first()
    if not scene_m:
        flash("Scene {} doesn't exist".format(scene_id), "error")
        return redirect(request.referrer, url_for("client.home"))
    filepath = os.path.join(SCREENSHOT_DIR, scene_m.screenshot.full_filename)
    try:
        os.remove(filepath)
        flash("Deleted screenshot file {}".format(scene_m.screenshot.id), "success")
    except Exception as e:
        flash(str(e), "error")
    db.session.delete(scene_m)
    db.session.delete(scene_m.screenshot)
    db.session.commit()
    flash("Scene {} and screenshot deleted".format(scene_id), "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/time_description/<int:time_description_id>/edit", methods=["POST"])
def time_description_edit(time_description_id):
    time_description_m = TimeDescription.query.filter_by(id=time_description_id).first()
    if not time_description_m:
        flash("Time_description {} doesn't exist".format(time_description_id), "error")
        return redirect(request.referrer, url_for("client.home"))
    description = request.form.get("description")
    time_description_m.description = description
    db.session.add(time_description_m)
    db.session.commit()
    flash("Time_description {} edited".format(time_description_id), "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/time_descriptions/<int:time_description_id>/delete", methods=["GET", "POST"])
def time_description_delete(time_description_id):
    time_description_m = TimeDescription.query.filter_by(id=time_description_id).first()
    if not time_description_m:
        flash("Time_description {} doesn't exist".format(time_description_id), "error")
        return redirect(request.referrer, url_for("client.home"))
    filepath = os.path.join(SCREENSHOT_DIR, time_description_m.screenshot.full_filename)
    try:
        os.remove(filepath)
        flash("Deleted screenshot file {}".format(time_description_m.screenshot.id), "success")
    except Exception as e:
        flash(str(e), "error")
    db.session.delete(time_description_m)
    db.session.delete(time_description_m.screenshot)
    db.session.commit()
    flash("Time_description {} and screenshot deleted".format(time_description_id), "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/screenshots/<int:screenshot_id>/delete", methods=["GET", "POST"])
def screenshot_delete(screenshot_id):
    screenshots_m = Screenshot.query.filter_by(id=screenshot_id).first()
    if not screenshots_m:
        flash("Screenshot {} doesn't exist".format(screenshot_id), "error")
        return redirect(request.referrer, url_for("client.home"))
    filepath = os.path.join(SCREENSHOT_DIR, screenshots_m.full_filename)
    try:
        os.remove(filepath)
        flash("Deleted screenshot file {}".format(screenshots_m.id), "success")
    except Exception as e:
        flash(str(e), "error")
    db.session.delete(screenshots_m)
    db.session.commit()
    flash("Screenshot {} deleted".format(screenshot_id), "success")
    return redirect(request.referrer or url_for("client.home"))


@bp_client.route("/log_scroll")
def log_scroll():
    return render_template("log_scroll.html", time=3600)  # 1*3600+13*60+23)


def worker_start_task(task_name, task_kwargs):
    async_task = worker_app.send_task(task_name, [], task_kwargs)
    task = Task(name=task_name,
                uuid=async_task.id,
                kwargs=json.dumps(task_kwargs),
                )
    db.session.add(task)
    db.session.commit()
    return async_task.id


@bp_client.route('/tasks/<task_name>/start', methods=['GET', 'POST'])
def start_task(task_name):
    task_kwargs = {k: v for k, v in request.form.items() if k != 'csrf_token'}
    async_task_id = worker_start_task(task_name, task_kwargs)
    return redirect(url_for('client.get_task_status', task_uuid=async_task_id))


def get_task_ctx(task_uuid):
    ctx = {}
    async_task = worker_app.AsyncResult(id=task_uuid)
    ctx['async_task'] = {
        'result': async_task.result,
        'status': async_task.status,
    }
    task = Task.query.filter_by(uuid=async_task.id).first()
    ctx['task'] = task.serialize()
    out_filename = os.path.join(TASKS_BUF_DIR, task_uuid)
    if os.path.exists(out_filename):
        with open(out_filename) as f:
            ctx['async_task']['stdout'] = f.read()
    return ctx


class TasksTable(Table):
    uuid = LinkCol('Task', "client.get_task_status", url_kwargs=dict(task_uuid='uuid'))
    name = Col('name')


@bp_client.route("/tasks", methods=["GET"])
def list_tasks():
    tasks = Task.query.all()
    tasks_table = TasksTable(tasks)
    return render_template("tasks.html", tasks_table=tasks_table)


@bp_client.route('/tasks/<task_uuid>/status')
def get_task_status(task_uuid):
    ctx = get_task_ctx(task_uuid)
    return render_template('task.html', **ctx)


@bp_client.route('/tasks/<task_id>')
def get_async_task_result(task_id):
    ctx = get_task_ctx(task_id)
    return jsonify(ctx)


PROCESSES = {}


def start_tail_process(filename):
    return subprocess.Popen(['tail', '-F', filename],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT,
                            universal_newlines=True,
                            )


def tail(process):
    for line in iter(process.stdout.readline, b''):
        line = line.strip()
        # print("sending s_tail: {}".format(line))
        emit("s_tail", {"stdout": line})
        # print("sent s_tail: {}".format((line)))


def kill_process(process):
    if process:
        # print('process: killing {}'.format(process))
        process.kill()
        print('process: killed {}'.format(process))


def remove_process(uuid):
    if uuid in PROCESSES:
        del PROCESSES[uuid]


@socketio.on('connect')
def handle_connect():
    sid = str(request.sid)
    print("socket.connect: session id: {}".format(sid))
    emit('s_connect', {
        "sid": sid,
    })


@socketio.on('start_tail')
def handle_start_tail(json):
    sid = str(request.sid)
    task_uuid = json.get("task_uuid")
    task_out_filename = os.path.join(TASKS_BUF_DIR, task_uuid)
    process = start_tail_process(task_out_filename)
    PROCESSES[sid] = ({
        "sid": sid,
        "process": process,
    })
    print("socket.start_tail: session id: {}".format(sid))
    emit('s_start_tail', {
        "sid": sid,
        "task_uuid": task_uuid,
    })
    tail(process)


@socketio.on('disconnect')
def handle_disconnect():
    sid = str(request.sid)
    process = PROCESSES.get(sid, {}).get("process")
    print("socket.disconnect: session id: {}".format(sid))
    kill_process(process)
    remove_process(process)
    emit('s_dissconnect', {
        "sid": sid,
    })


@bp_client.route("/pipeline")
def pipeline():
    pass
