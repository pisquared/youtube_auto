# Автоматизиране на задачи за канала на Curious Bunch

Уебсървъра разполага със следните страници:

* Import - сваля клипче от ютуб / копира / мести от локална директория
* TimeIt - таймване на клипчето (помага с description генерирането)
* Cut - отрязване на сегмент от видеото
* Intro/Outro - добавяне на интро/outro от двете страни на клипа
* Thumbnail - генериране на Thumbnail
* Upload - Качване на видеото в ютуб
* Extract Audio - TODO

## Screenshots
![download page](screenshots/download.png)
![cut page](screenshots/cut.png)
![timeit page](screenshots/timeit.png)
![intro_outr page](screenshots/intro_outro.png)
![thumbnail page](screenshots/thumbnail.png)
![upload page](screenshots/upload.png)


## Инсталация

1. Питонец-неща

```
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

2. За убунту-базирани системи, имаме нужда и от (поне):

```
sudo apt install ffmpeg
```

\[за други операционни системи - еквивалентни пакети\]

3. Аутентикация с [гугълски неща](https://developers.google.com/youtube/v3/quickstart/python#step_1_set_up_your_project_and_credentials):

```
Create or select a project in the API Console. Complete the following tasks in the API Console for your project:

In the library panel, search for the YouTube Data API v3. Click into the listing for that API and make sure the API is enabled for your project.

In the credentials panel, create two credentials:

Create an API key You will use the API key to make API requests that do not require user authorization. For example, you do not need user authorization to retrieve information about a public YouTube channel.

Create an OAuth 2.0 client ID Set the application type to Other. You need to use OAuth 2.0 credentials for requests that require user authorization. For example, you need user authorization to retrieve information about the currently authenticated user's YouTube channel.

Download the JSON file that contains your OAuth 2.0 credentials. The file has a name like client_secret_CLIENTID.json, where CLIENTID is the client ID for your project.
```

4. Началната папка за data (съдържа картинки и интро/outro видеа) може да се свали от [тук](https://drive.google.com/open?id=1dTPYsGz0hxEtqtKTDc2o8Ei22SaIZNPp) и после:

```
tar xfvz data.tar.gz
rm data.tar.gz
```

## Running

```
source venv/bin/activate
python run.py
# in seperate terminal
./start_worker.sh
```

## TODO
* [SECURITY] Filename/paths not safe for production!!
* [PERFORMANCE] Background jobs (celery, redis/rabbitmq etc.)
* [READIBILITY] Split app.py into multiple files, DRY it up a bit
* [fonts] auto generate fonts on system on first start
* [timeit] xhr
* [cut] smile detection - has some rudimentary implementation
* [thumbnail] add options for more "watermarks"
* [thumbnail] add types of thumbnail (e.g. for news/per playlist etc)
* [upload] description templates
* [upload] schedule for later (using tww?)
* [extract audio] -> another ffmpeg command, should be quite quick
* [auto create blog post] -> squarespace *doesn't* provide API... so... f'em?
* [pipeline] automate some of these tasks in a pipeline (e.g. if a video is already timed, schedule the cuts, titles and descriptions -> each Intro/Outro -> each Thumbnail gen -> each upload and schedule a date )

### ver 0.2
* [import] people should be able to upload videos to the platform itself (i.e. remove the local path option and provide an upload button)
* [user auth] authenticate users so that they can upload their own videos and have playlists that they manage with schedules to remind them
* [youtube api] automate the videos spreadsheet - which podcasts are to be cut and uploaded etc + list the videos in the channel that are remote, when are the next ones scheduled, easy "download" button
 

## Други скриптове :
* `./grab_bg.sh input.mp4` - Генерира 3 картинки от инпут видео - на 25%, 50% и 75% от видеото
* `./gen_thumb.sh "Заглавие" bg.png` - Генерира thumbnail.png със Заглавие и `bg.png` на заден фон
Следните задачи са на "един клик разстояние":
