#!/bin/bash
convert -size 2000x1135 xc:none cb_logo.png -geometry +0+600 -composite blank.png
convert blank.png globe.png -geometry +1150+50 -composite blank_globe.png
convert -background '#261' -size 1400x300 -fill '#eee' -font Arial-Black -gravity center \
            label:"$1" blank_globe.png  +swap  \
            -gravity northwest -geometry +640+560 -composite  title.png
convert $2 -resize 2000x1135^ -gravity center -extent 2000x1135 bg_r.png
convert bg_r.png title.png -composite thumbnail.png
rm bg_r.png title.png blank.png blank_globe.png
