#!/bin/bash

rm bg_25.png bg_50.png bg_75.png

duration=`ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $1`;

pos25=$(echo $duration 0.25 | awk '{printf "%4.0f\n",$1*$2}')
pos50=$(echo $duration 0.50 | awk '{printf "%4.0f\n",$1*$2}')
pos75=$(echo $duration 0.75 | awk '{printf "%4.0f\n",$1*$2}')

ffmpeg -ss $pos25 -i $1 -vf thumbnail -vsync 0 -vframes 1 -q:v 2 bg_25.png 2>/dev/null
ffmpeg -ss $pos50 -i $1 -vf thumbnail -vsync 0 -vframes 1 -q:v 2 bg_50.png 2>/dev/null
ffmpeg -ss $pos75 -i $1 -vf thumbnail -vsync 0 -vframes 1 -q:v 2 bg_75.png 2>/dev/null

