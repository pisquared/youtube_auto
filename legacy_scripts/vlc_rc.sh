#!/usr/bin/env bash

vlc -I rc --rc-host "localhost:1234" /usr/local/google/home/pisquared/cb/ff/ff_4d_bloopers.mkv
echo 'pause' | netcat -N localhost 1234
echo 'add /usr/local/google/home/pisquared/cb/ff/ff_4d_bloopers.mkv' | netcat -N localhost 1234
echo 'play' | netcat -N localhost 1234