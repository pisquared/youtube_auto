#!/bin/bash

#for i in "$@"
#do
#case $i in
#    -e=*|--extension=*)
#    EXTENSION="${i#*=}"
#    shift # past argument=value
#    ;;
#    -s=*|--searchpath=*)
#    SEARCHPATH="${i#*=}"
#    shift # past argument=value
#    ;;
#    -l=*|--lib=*)
#    LIBPATH="${i#*=}"
#    shift # past argument=value
#    ;;
#    --default)
#    DEFAULT=YES
#    shift # past argument with no value
#    ;;
#    *)
#          # unknown option
#    ;;
#esac
#done
#echo "FILE EXTENSION  = ${EXTENSION}"
#echo "SEARCH PATH     = ${SEARCHPATH}"
#echo "LIBRARY PATH    = ${LIBPATH}"
#echo "DEFAULT         = ${DEFAULT}"
#echo "Number files in SEARCH PATH with EXTENSION:" $(ls -1 "${SEARCHPATH}"/*."${EXTENSION}" | wc -l)
#if [[ -n $1 ]]; then
#    echo "Last line of file specified as non-opt/last argument:"
#    tail -1 $1
#fi

set -x
set -e

LABEL=$1
BG_FILENAME="${2:-bg.png}"
LABEL_BG_COLOR="${3:-#e30}"
LABEL_POS="${4:-+640+670}"
LABEL_SIZE="${5:-1350x300}"
CB_POS="${6:-+0+560}"

#echo $LABEL
#echo $BG_FILENAME
#echo $LABEL_BG_COLOR
#echo $LABEL_POS
#echo $LABEL_SIZE
#echo $CB_POS

convert -size 2000x1135 xc:none data/serve/images/cb_logo.png -geometry "${CB_POS}" -composite blank.png
convert -background "${LABEL_BG_COLOR}" -size ${LABEL_SIZE} -fill '#eee' -font Arial-Black -gravity center \
            label:"$LABEL" blank.png  +swap  \
            -gravity northwest -geometry "${LABEL_POS}" -composite  title.png
convert $BG_FILENAME -resize 2000x1135^ -gravity center -extent 2000x1135 bg_r.png
convert bg_r.png title.png -composite thumbnail.png
rm bg_r.png title.png blank.png
