import os

basepath = os.path.dirname(os.path.realpath(__file__))

DATA_DIR = os.path.join(basepath, "data")
TASKS_DIR = os.path.join(DATA_DIR, "tasks")
TASKS_IN_DIR = os.path.join(TASKS_DIR, "in")
TASKS_PROC_DIR = os.path.join(TASKS_DIR, "proc")
TASKS_BUF_DIR = os.path.join(TASKS_DIR, "buf")
SERVE_DIR = os.path.join(DATA_DIR, "serve")
DATABASE_FILE = os.path.join(DATA_DIR, "db.sqlite")
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(DATABASE_FILE)

HAAR_DIR = os.path.join(DATA_DIR, "haar")
TMP_DIR = os.path.join(DATA_DIR, "tmp")
VIDEO_DIR_NAME = "videos"
VIDEO_DIR = os.path.join(SERVE_DIR, VIDEO_DIR_NAME)
VIDEO_TEMPLATES_DIR_NAME = "video_templates"
VIDEO_TEMPLATES_DIR = os.path.join(SERVE_DIR, VIDEO_TEMPLATES_DIR_NAME)
IMAGES_DIR_NAME = "images"
IMAGES_DIR = os.path.join(SERVE_DIR, IMAGES_DIR_NAME)
SCREENSHOT_DIR_NAME = "screenshots"
SCREENSHOT_DIR = os.path.join(SERVE_DIR, SCREENSHOT_DIR_NAME)
THUMBNAIL_DIR_NAME = "thumbnails"
THUMBNAIL_DIR = os.path.join(SERVE_DIR, THUMBNAIL_DIR_NAME)
AUDIO_DIR_NAME = "audios"
AUDIO_DIR = os.path.join(SERVE_DIR, AUDIO_DIR_NAME)
SCENE_DIR_NAME = "scenes"
SCENE_DIR = os.path.join(SERVE_DIR, SCENE_DIR_NAME)

MAKEDIRS = [
    DATA_DIR, TASKS_IN_DIR, TASKS_PROC_DIR, TASKS_BUF_DIR, TMP_DIR, IMAGES_DIR, SCREENSHOT_DIR, THUMBNAIL_DIR,
    AUDIO_DIR, VIDEO_DIR,
    SCENE_DIR
]

DEFAULT_CATEGORY = "28"  # science and technology; 27 is education
DEFAULT_KEYWORDS = "образование,любопитен,физика,астрономия,компютри,олимпиади,любопитковци,science,technology,livestream,physics,astronomy,astrophysics,computers,computer science,quantum,black holes,curiousity,nerd,geek,live events,networking"
